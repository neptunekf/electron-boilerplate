const { app, desktopCapturer, Menu, screen } = require('electron');

const fs = require('fs')
const path = require('path')


function takeScreenshot() {
	console.log('Gathering screens...')
	const thumbSize = determineScreenShotSize()
	const options = { types: ['screen'], thumbnailSize: thumbSize }

	desktopCapturer.getSources(options).then((sources) => {
		console.log('Sources received:'+sources.length);
		sources.forEach(function (source) {
			const sourceName = source.name.toLowerCase();
			console.log(sourceName);
			if ( ['entire screen', 'screen 1'].includes(sourceName)) {
				const screenshotPath = path.join(app.getPath('pictures'), 'screenshot-' + new Date().toJSON() + '.png')
				fs.writeFile(screenshotPath, source.thumbnail.toPNG(), function (error) {
					if (error) return console.log(error)
					console.log('Saved screenshot to: ' + screenshotPath);
				})
			}
		})
	}).catch(console.error);
}

function determineScreenShotSize() {
	var screenSize = screen.getPrimaryDisplay().workAreaSize
	return {
		width: screenSize.width,
		height: screenSize.height
	}
}

const template = [
	{
		label: 'Capture',
		click() {
			takeScreenshot()
		}
	},
	{
		role: 'quit',
	}
];

setInterval(takeScreenshot, 15000);

module.exports = Menu.buildFromTemplate(template);
